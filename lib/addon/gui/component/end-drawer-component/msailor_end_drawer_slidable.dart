import 'package:flutter/material.dart';

import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:msailor/data/data.dart';
import 'package:msailor/model/media_info.dart';
import 'package:msailor/service/service.dart';

class MsailorEndDrawerSlidable extends StatefulWidget {
  final MediaInfo mediaInfo;

  MsailorEndDrawerSlidable(this.mediaInfo);

  @override
  State<StatefulWidget> createState() =>
      _MsailorEndDrawerSlidableState(this.mediaInfo);
}

class _MsailorEndDrawerSlidableState extends State<MsailorEndDrawerSlidable> {
  MediaInfo _mediaInfo;

  _MsailorEndDrawerSlidableState(this._mediaInfo);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
        color: Colors.grey[900],
        child: ListTile(
          leading: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.network(this._mediaInfo.videoPicUrl!,
                  key: null,
                  width: MediaQuery.of(context).size.width / 20,
                  height: MediaQuery.of(context).size.height / 20)),
          title: Row(
            children: [
              Flexible(child: Text(this._mediaInfo.title!)),
              Visibility(
                  visible: this._mediaInfo.live!,
                  child: Image.asset(
                    'asset/gif/live.gif',
                    scale: 7,
                  )),
            ],
          ),
          subtitle: Row(
            children: [
              CircleAvatar(
                radius: 10,
                backgroundImage:
                    NetworkImage(this._mediaInfo.channelAvatarUrl!),
              ),
            ],
          ),
        ),
      ),
      key: Key(this._mediaInfo.id!),
      dismissal: SlidableDismissal(
        key: Key(this._mediaInfo.id!),
        dragDismissible: true,
        child: SlidableDrawerDismissal(),
        onDismissed: (direction) {
          Data().memory.mediaQueue.remove(this._mediaInfo);
        },
      ),
      actions: <Widget>[
        IconSlideAction(
          caption: 'Drag to remove',
          color: Colors.red,
          icon: Icons.highlight_remove,
        ),
      ],
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Favorite',
          color: Colors.grey[900],
          icon: Icons.star,
          onTap: () => _showSnackBar('favorite'),
        ),
        IconSlideAction(
          caption: 'Add to list',
          color: Colors.grey[900],
          icon: Icons.playlist_add,
          onTap: () => _showSnackBar('addToList'),
        ),
        Visibility(
            visible: !this._mediaInfo.live!,
            child: IconSlideAction(
              caption: 'Download',
              color: Colors.grey[900],
              icon: Icons.download_sharp,
              onTap: () {
                if (!this._mediaInfo.live!)
                  Service().msailor.youtubeDownload(this._mediaInfo);
              },
            )),
      ],
    );
  }

  _showSnackBar(String temp) {}
}
