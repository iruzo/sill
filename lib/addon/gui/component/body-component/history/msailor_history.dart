import 'dart:io';

import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/body-component/msailor_body_slidable.dart';
import 'package:msailor/service/service.dart';
import 'package:msailor/data/data.dart';
import 'package:msailor/model/media_info.dart';

class MsailorHistory extends StatefulWidget {
  MsailorHistory(this._historyType, this._historyPath);

  String _historyType;
  String _historyPath;

  @override
  State<StatefulWidget> createState() =>
      _MsailorHistoryState(this._historyType, this._historyPath);
}

class _MsailorHistoryState extends State<MsailorHistory> {
  _MsailorHistoryState(this._historyType, this._historyPath);

  String _historyType;
  String _historyPath;

  Future<ListView>? _history;
  String? _date;
  String? _entry;

  @override
  void initState() {
    super.initState();
    this._history = _getHistory();
  }

  void _refresh() {
    Future<ListView> newValue = _getHistory(
        date: this._date == null || this._date!.isEmpty ? null : this._date,
        entry:
            this._entry == null || this._entry!.isEmpty ? null : this._entry);
    setState(() {
      this._history = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        height: 70,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Icon(Icons.history),
                title: TextField(
                  decoration: InputDecoration(
                      hintText: 'Date (e.g. "2020-00-00 00:00:00.000Z")'),
                  onChanged: (String date) {
                    this._date = date;
                    _refresh();
                  },
                ),
              ),
            ),
            Expanded(
              child: ListTile(
                leading: Icon(Icons.list),
                title: TextField(
                  decoration: InputDecoration(hintText: 'Entry'),
                  onChanged: (String entry) {
                    this._entry = entry;
                    _refresh();
                  },
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.delete_forever),
              color: Colors.red,
              onPressed: () {
                Service().history.delete(
                    Data().configuration.historyPath() +
                        Platform.pathSeparator +
                        this._historyType,
                    entry: this._entry!,
                    date: this._date!);
              },
            ),
          ],
        ),
      ),
      Expanded(
          child: FutureBuilder<ListView>(
        future: this._history,
        builder: (context, AsyncSnapshot<ListView> snapshot) {
          return snapshot.hasData
              ? snapshot.data!
              : CircularProgressIndicator();
        },
      ))
    ]);
  }

  Future<ListView> _getHistory({String? date, String? entry}) async {
    List<String> history = (await Service()
        .history
        .read(this._historyPath, entry: entry, date: null));

    if (date != null)
      history = history
          .where((current) => current.split(']')[0].contains(date))
          .toList();

    return ListView(
        children: history
            .map((e) => MsailorBodySlidable(MediaInfo(
                source: this._historyType,
                id: '',
                url: '',
                live: false,
                channelName: '',
                channelSubDirUrl: '',
                channelAvatarUrl: '',
                title: e,
                videoPicUrl: this._historyType.contains('search')
                    ? 'http://icons.iconarchive.com/icons/google/noto-emoji-objects/1024/62861-blue-book-icon.png'
                    : e.contains('[downloading]-')
                        ? 'https://cdn.dribbble.com/users/68238/screenshots/759129/bluebutton.gif'
                        : 'https://cdn2.iconfinder.com/data/icons/flat-icons-web/40/OK-512.png',
                length: '',
                views: '')))
            .toList());
  }
}
