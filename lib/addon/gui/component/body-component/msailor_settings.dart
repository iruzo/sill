import 'package:flutter/material.dart';
import 'package:msailor/data/data.dart';

class MsailorSettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Icon(Icons.list),
          title: TextField(
            decoration:
                InputDecoration(hintText: Data().configuration.logPath()),
            onChanged: (String path) {
              Data().configuration.logPath(
                  value: path.isEmpty ? Data().configuration.logPath() : path);
            },
          ),
        ),
        ListTile(
          leading: Icon(Icons.history),
          title: TextField(
            decoration:
                InputDecoration(hintText: Data().configuration.historyPath()),
            onChanged: (String path) {
              Data().configuration.historyPath(
                  value:
                      path.isEmpty ? Data().configuration.historyPath() : path);
            },
          ),
        ),
        ListTile(
          leading: Icon(Icons.library_music),
          title: TextField(
            decoration:
                InputDecoration(hintText: Data().configuration.libraryPath()),
            onChanged: (String path) {
              Data().configuration.libraryPath(
                  value:
                      path.isEmpty ? Data().configuration.libraryPath() : path);
            },
          ),
        ),
        Text('\n'),
        MaterialButton(
          color: Colors.red,
          textColor: Colors.white,
          onPressed: () {
            Data().configuration.resetPropertiesFile();
          },
          child: Text('Reset settings'),
        ),
      ],
    );
  }
}
