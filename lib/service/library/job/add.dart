import 'dart:io';
import 'dart:convert';

import 'package:msailor/core/logic.dart';

/// Add entry to library.
///
/// * **author**: iruzo
/// * **params**:
///   * libraryFolderPath (e.g. /library)
///   * entry (json as string)
///   * listName (optional - name of the list you want to be added).
///   * [OPTIONAL] pathToCopyFrom (path to file you want to add to the library).
/// * **return**: void.
class Add extends Logic<void> {
  /// Add entry to library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * entry (normal String or JSON)
  ///   * listName (name of the list you want to be added).
  ///   * [OPTIONAL] pathToCopyFrom (path to file you want to add to the library).
  /// * **return**: instance.
  Add(this._libraryFolderPath, this._entry, this._listName,
      {this.pathToCopyFrom});

  String _entry;
  String _libraryFolderPath;
  String _listName;
  String? pathToCopyFrom;

  @override
  Future<void> run() async {
    if (this.pathToCopyFrom != null) {
      Directory fileDirectory =
          Directory(this._libraryFolderPath + Platform.pathSeparator + 'file');
      if (!fileDirectory.existsSync())
        fileDirectory.createSync(recursive: true);
      File(this.pathToCopyFrom!).copy(this._libraryFolderPath +
          Platform.pathSeparator +
          'file' +
          Platform.pathSeparator +
          this._entry);
    }

    Directory listDirectory =
        Directory(this._libraryFolderPath + Platform.pathSeparator + 'list');
    if (!listDirectory.existsSync()) listDirectory.createSync(recursive: true);
    File listFile =
        File(listDirectory.path + Platform.pathSeparator + this._listName);

    Map<String, dynamic> decodedJSON;
    if (!listFile.existsSync()) {
      listFile.createSync(recursive: true);
      decodedJSON = {this._getDateTime(): this._entry};
    } else {
      decodedJSON =
          json.decode(listFile.readAsStringSync()) as Map<String, dynamic>;
      decodedJSON.addAll({this._getDateTime(): this._entry});
    }
    String encodedJSON = json.encode(decodedJSON);
    listFile.writeAsStringSync(encodedJSON);
  }

  String _getDateTime() {
    DateTime dateTime = new DateTime.now();
    return dateTime.toString();
  }
}
