import 'dart:io';
import 'dart:convert';

import 'package:msailor/core/logic.dart';

/// Read entry in library.
///
/// * **author**: iruzo
/// * **params**:
///   * libraryFolderPath (e.g. /library)
///   * [OPTIONAL] entry (just String inside de value saved in a list)
///   * [OPTIONAL] listName
///   * [OPTIONAL] file (if checked, it will return the file with entry as name, and if there is no entry, it return all files)
///     * **How this works**
///     * If you set entry and list, the entry will be returned from that list.
///     * If you only set entry, the entry will be returned from every list.
///     * If you only set list, the entire list will be returned.
///     * If entry and list are null, all lists will be returned.
/// * **return**: Map<String, dynamic> where 'dynamic' .
/// * **WARNING**: The files will be returned as Map<String, dynamic> inside the value, not the key.
class Read extends Logic<Map<String, dynamic>> {
  /// Read entry in library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * [OPTIONAL] entry (just String inside de value saved in a list)
  ///   * [OPTIONAL] listName
  ///   * [OPTIONAL] file (if checked, it will return the file with entry as name, and if there is no entry, it return all files)
  ///     * **How this works**
  ///     * If you set entry and list, the entry will be returned from that list.
  ///     * If you only set entry, the entry will be returned from every list.
  ///     * If you only set list, the entire list will be returned.
  ///     * If entry and list are null, all lists will be returned.
  /// * **return**: instance.
  /// * **WARNING**: The files will be returned as Map<String, dynamic> inside the value, not the key.
  Read(this._libraryFolderPath, {this.entry, this.listName, this.file});

  String _libraryFolderPath;
  String? entry;
  String? listName;
  bool? file;

  @override
  Future<Map<String, dynamic>> run() async {
    Directory listsDirectory =
        Directory(this._libraryFolderPath + Platform.pathSeparator + 'list');
    Map<String, dynamic> result = {};

    if (this.file == null) {
      if (listsDirectory.existsSync()) {
        try {
          result = json.decode(this.entry!);
          this.entry = this.entry!.replaceAll('\"', '\\"');
        } on FormatException {}

        if (this.entry != null && this.listName == null) {
          // get entry
          listsDirectory.listSync().forEach((list) {
            if ((list as File).readAsStringSync().contains(this.entry!)) {
              Map<String, dynamic> decodedJSON =
                  json.decode(list.readAsStringSync()) as Map<String, dynamic>;
              decodedJSON.forEach((key, value) {
                if ((value as String).contains(this.entry!)) {
                  try {
                    result = json.decode(value);
                  } on FormatException {
                    result = {key: value};
                  }
                }
              });
            }
          });
        }

        if (this.entry != null && this.listName != null) {
          // get entry from list
          listsDirectory
              .listSync()
              .where((list) => list.path.contains(this.listName!))
              .forEach((list) {
            if ((list as File).readAsStringSync().contains(this.entry!)) {
              Map<String, dynamic> decodedJSON =
                  json.decode(list.readAsStringSync()) as Map<String, dynamic>;
              decodedJSON.forEach((key, value) {
                if ((value as String).contains(this.entry!)) {
                  try {
                    result = json.decode(value);
                  } on FormatException {
                    result = {key: value};
                  }
                }
              });
            }
          });
        }

        if (this.entry == null && this.listName != null) {
          // get list
          listsDirectory
              .listSync()
              .where((list) => list.path.contains(this.listName!))
              .forEach((list) {
            Map<String, dynamic> decodedJSON =
                json.decode((list as File).readAsStringSync())
                    as Map<String, dynamic>;
            decodedJSON.forEach((key, value) {
              try {
                result.addAll(json.decode(value) as Map<String, dynamic>);
              } on FormatException {
                result.addAll({key: value});
              }
            });
          });
        }

        if (this.entry == null && this.listName == null) {
          // get all lists
          listsDirectory.listSync().forEach((list) {
            Map<String, dynamic> decodedJSON =
                json.decode((list as File).readAsStringSync())
                    as Map<String, dynamic>;
            decodedJSON.forEach((key, value) {
              try {
                result.addAll(json.decode(value) as Map<String, dynamic>);
              } on FormatException {
                result.addAll({key: value});
              }
            });
          });
        }

        return result;
      }
    } else if (this.file == true) {
      Directory filesDirectory =
          Directory(this._libraryFolderPath + Platform.pathSeparator + 'file');

      if (filesDirectory.existsSync()) {
        if (this.entry != null) {
          // get file
          filesDirectory
              .listSync()
              .where((file) => file.path.contains(this.entry!))
              .forEach((file) {
            result.addAll({this.entry!: file});
          });
        }

        if (this.entry == null && this.listName == null) {
          // get all files
          int ii = 0;
          filesDirectory.listSync().forEach((file) {
            result.addAll({ii.toString(): file});
            ii++;
          });
        }

        return result;
      }
    }

    throw Exception();
  }
}
