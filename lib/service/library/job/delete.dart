import 'dart:io';
import 'dart:convert';

import 'package:msailor/core/logic.dart';

/// Delete entry in library.
///
/// * **author**: iruzo
/// * **params**:
///   * libraryFolderPath (e.g. /library)
///   * [OPTIONAL] entry (just String inside de value saved in a list)
///   * [OPTIONAL] listName
///   * [OPTIONAL] removeFile (if checked, it will delete the file with entry as name, and if there is no entry, it will delete all files)
///     * **How this works**
///     * If you set entry and list, the entry will be deleted from that list.
///     * If you only set entry, the entry will be deleted from every list.
///     * If you only set list, the entire list will be deleted.
///     * If entry and list are null, all lists will be deleted.
/// * **return**: void.
class Delete extends Logic<void> {
  /// Delete entry in library.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * libraryFolderPath (e.g. /library)
  ///   * [OPTIONAL] entry (just String inside de value saved in a list)
  ///   * [OPTIONAL] listName
  ///   * [OPTIONAL] removeFile (if checked, it will delete the file with entry as name, and if there is no entry, it will delete all files)
  ///     * **How this works**
  ///     * If you set entry and list, the entry will be deleted from that list.
  ///     * If you only set entry, the entry will be deleted from every list.
  ///     * If you only set list, the entire list will be deleted.
  ///     * If entry and list are null, all lists will be deleted.
  /// * **return**: instance.
  Delete(this._libraryFolderPath, {this.entry, this.listName, this.removeFile});

  String _libraryFolderPath;
  String? entry;
  String? listName;
  bool? removeFile;

  @override
  Future<void> run() async {
    Directory listsDirectory =
        Directory(this._libraryFolderPath + Platform.pathSeparator + 'list');

    if (listsDirectory.existsSync()) {
      if (this.entry != null && this.listName == null) {
        // remove entry
        listsDirectory.listSync().forEach((list) {
          this.removeEntryFromList((list as File));
        });
      }

      if (this.entry != null && this.listName != null) {
        // remove entry from list
        listsDirectory
            .listSync()
            .where((list) => list.path.contains(this.listName!))
            .forEach((list) {
          this.removeEntryFromList((list as File));
        });
      }

      if (this.entry == null && this.listName != null) {
        // remove list
        File listFile =
            File(listsDirectory.path + Platform.pathSeparator + this.listName!);
        if (listFile.existsSync()) listFile.delete();
      }

      if (this.entry == null && this.listName == null) {
        // remove all lists
        listsDirectory.delete(recursive: true);
      }
    }

    Directory filesDirectory =
        Directory(this._libraryFolderPath + Platform.pathSeparator + 'file');
    if (filesDirectory.existsSync()) {
      if (this.entry != null &&
          this.removeFile != null &&
          this.removeFile == true) {
        // remove file
        File fileToBeDeleted =
            File(filesDirectory.path + Platform.pathSeparator + this.entry!);
        if (fileToBeDeleted.existsSync()) fileToBeDeleted.delete();
      }

      if (this.entry == null &&
          this.listName == null &&
          this.removeFile != null &&
          this.removeFile == true) {
        // remove all files
        filesDirectory.delete(recursive: true);
      }
    }
  }

  void removeEntryFromList(File list) {
    if (list.readAsStringSync().contains(this.entry!)) {
      Map<String, dynamic> decodedJSON =
          json.decode(list.readAsStringSync()) as Map<String, dynamic>;
      List<String> positionKeyToRemove = [];
      decodedJSON.forEach((key, value) {
        if ((value as String).contains(this.entry!))
          positionKeyToRemove.add(key);
      });
      positionKeyToRemove.forEach((key) {
        decodedJSON.remove(key);
      });
      list.writeAsStringSync(json.encode(decodedJSON));
    }
  }
}
