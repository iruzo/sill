// import 'package:flutter/material.dart';
import 'package:msailor/service/history/history.dart';
import 'package:msailor/service/library/library.dart';
import 'package:msailor/service/net/net.dart';
import 'package:msailor/service/msailor/msailor.dart';
// import 'package:msailor/submodules/interfaces/gui/Gui.dart';

/// Manage the calls to the functionalities of the logical part of the application.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Service {
  /// Manage the calls to the functionalities of the logical part of the application.
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Service();

  /// Msailor functionalities
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Msailor msailor = Msailor();

  /// Network functionalities
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Net net = Net();

  /// History manager
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: void.
  History history = History();

  /// Library manager
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: void.
  Library library = Library();

  /// Start the app.
  ///
  /// **author**: iruzo
  /// **params**: void.
  /// **return**: void.
  void init() {
    //TODO
  }
}
