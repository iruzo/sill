import 'package:msailor/core/logic.dart';

/// Get channel name from channel url.
///
/// * **author**: iruzo
/// * **params**: Youtube channel url.
/// * **return**: Channel name as String.
class YoutubeChannelName extends Logic<String> {
  /// Get channel name from channel url.
  ///
  /// * **author**: iruzo
  /// * **params**: Youtube channel url.
  /// * **return**: Channel name as String.
  YoutubeChannelName(this._channelUrl);

  String _channelUrl;

  @override
  Future<String> run() async {
    String scrappedPage =
        await this.service.net.webScrapping(this._channelUrl, 'body');
    scrappedPage = scrappedPage.split('"name": "')[1];
    scrappedPage = scrappedPage.split('"}')[0];

    return scrappedPage;
  }
}
