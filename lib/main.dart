import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/gui.dart';
import 'package:msailor/service/service.dart';

void main() {
  Service().init();
  runApp(Gui());
}
