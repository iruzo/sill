import 'package:msailor/core/calls.dart';
import 'package:msailor/core/log.dart';

import 'runner.dart';

/// Use cases should extends from this class, take in mind that if the use case return void, this class shoud be typed 'void'.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
abstract class Logic<T> extends Calls implements Runner<T> {
  /// Execute the functionality.
  ///
  /// * **params**: void.
  /// * **return**: void.
  Future<T> call() async {
    var _log = Log();
    _log.debug("[START] " + this.runtimeType.toString() + "()");

    if (T.toString() == "void") {
      return this.run().then((value) {
        _log.debug("[END  ] " +
            this.runtimeType.toString() +
            "()" +
            " - " +
            value.toString());
        return value;
      })
          // ignore: return_of_invalid_type_from_catch_error
          .catchError((e) => _log.error("[END  ] " +
              this.runtimeType.toString() +
              "()" +
              " - " +
              e.toString()));
    } else {
      return await this.run().then((value) {
        _log.debug("[END  ] " +
            this.runtimeType.toString() +
            "()" +
            " - " +
            value.toString());
        return value;
      })
          // ignore: return_of_invalid_type_from_catch_error
          .catchError((e) => _log.error("[END  ] " +
              this.runtimeType.toString() +
              "()" +
              " - " +
              e.toString()));
    }
  }

  //TODO iruzo - String toString() {}

}
